## Brave New Aeternum (1.15.2)
### Last Updated - 2/6/2020

## Instructions

Please follow the instructions properly or otherwise suffer the consequences.

### For Starters, Fresh Installation (where you never touched this game to begin with)
- Install Minecraft

### For Starters, Existing Installation (where you already have a .minecraft)
- Navigate to `.minecraft` folder
  - For Windows, press `WIN + R` and type `%APP_DATA%`
  - For macOS, I doubt you can even run this.
- Clear the existing contents, if any, by deleting it

### Rest of the Instructions
- Run `forge-1.15.2-31.1.0-installer.jar` and then `Install as Client`

  ![forge-installer](images/forge-installer.png)

- Once complete, navigate your way to your `.minecraft` folder
  - For Windows, press `WIN + R` and type `%APPDATA%`
  - For macOS, question whether you even have enough RAM to begin with

  ![run-as](images/run-as.png)

- Copy the `mods`, `config`, and `scripts` folder into your `.minecraft` folder
- Optional - I recommend `symlink`ing the three folders above to make updating easy:
  - Windows (using your respective drive and directories, of course):
    ```
    mklink /D C:\Users\<Your Username>\AppData\Roaming\.minecraft\mods C:\brave-new-aeternum\mods

    mklink /D C:\Users\<Your Username>\AppData\Roaming\.minecraft\config C:\brave-new-aeternum\config

    mklink /D C:\Users\<Your Username>\AppData\Roaming\.minecraft\scripts C:\brave-new-aeternum\scripts 
    ```
  - macOS (using your respective drive and directories, of course):
    ```
    WIP
    ```
- Launch Minecraft
- Press `Installations` at the top, press `... (More Options)`, and hit `Edit`

  ![launch-options](images/launch-options.png)

- On the following dialog, name the installation whatever you'd like and make sure that the version is set to `release 1.15.2-forge-31.1.0`
- Press `More Options`
- Under `JVM Arguments`, set memory settings to `5G` (minimum if you plan on using shaders, set it to `7G` or `8G`)

  ```
  -Xmx5G -XX:+UnlockExperimentalVMOptions -XX:+UseG1GC -XX:G1NewSizePercent=20 -XX:G1ReservePercent=20 -XX:MaxGCPauseMillis=50 -XX:G1HeapRegionSize=32M
  ```

  ![config-settings](images/config-settings.png)
- Press `Create`

- Go back to `Play` and then press the dropdown arrow next to the green Play button

  ![play-with-new-config](images/play-with-new-config.png)
  
- Select the newly created installation
- Run and play!
