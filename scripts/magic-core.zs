var magicCoreOne = <baublelicious:item.itemmagiccore>;
var magicCoreTwo = <baublelicious:item.itemmagiccore:1>;
var magicCoreThree = <baublelicious:item.itemmagiccore:2>;

var ironBlock = <minecraft:iron_block>;
var goldBlock = <minecraft:gold_block>;
var diamond = <minecraft:diamond>;

var notchGoldenApple = <minecraft:golden_apple:1>;
var diamondBlock = <minecraft:diamond_block>;

var netherStar = <minecraft:nether_star>;
var dragonBreath = <minecraft:dragon_breath>;

// Recipe Modification for Tier 1 - Magic Core
recipes.remove(magicCoreOne);
mods.jei.JEI.addDescription(magicCoreOne, "[I] = Iron Block", "[G] = Gold Block", "[D] = Diamond", "", "[I] [G] [I]", "[G] [D] [G]", "[I] [G] [I]");
recipes.addShaped("eidolonMagicCoreOne", magicCoreOne, [[ironBlock, goldBlock, ironBlock], [goldBlock, diamond, goldBlock], [ironBlock, goldBlock, ironBlock]]);

// Recipe Modification for Tier 2 - Magic Core
recipes.remove(magicCoreTwo);
mods.jei.JEI.addDescription(magicCoreTwo, "[A] = Notched (Purple) Golden Apple", "[D] = Diamond Block", "[M] = Magic Core Tier 1", "", "[A] [D] [A]", "[D] [M] [D]", "[A] [D] [A]");
recipes.addShaped("eidolonMagicCoreTwo", magicCoreTwo, [[notchGoldenApple, diamondBlock, notchGoldenApple], [diamondBlock, magicCoreOne, diamondBlock], [notchGoldenApple, diamondBlock, notchGoldenApple]]);

// Recipe Modification for Tier 3 - Magic Core
recipes.remove(magicCoreThree);
mods.jei.JEI.addDescription(magicCoreThree, "[D] = Dragon's Breath", "[N] = Nether Star", "[M] = Magic Core Tier 2", "", "[D] [N] [D]", "[N] [M] [N]", "[D] [N] [D]");
recipes.addShaped("eidolonMagicCoreThree", magicCoreThree, [[dragonBreath, netherStar, dragonBreath], [netherStar, magicCoreTwo, netherStar], [dragonBreath, netherStar, dragonBreath]]);
