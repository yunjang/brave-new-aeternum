### Version 1.0
#### Last Update: 9/9/2018

## Because this is adding dynamic lighting / shadows, as well as higher resolution textures and more graphically intensive things, please ensure your computer specs are up to par.
#### I recommend changing the JVM argument parameter `-Xmx5G` to at least `-Xmx8G`, assuming you have the RAM for it.
#### If your computer is running too hot to too slow, you can choose to disable either the shaders or the textures. Or just not use either.

#

In order to make Minecraft look prettier, you will need to complete the following:

1. Install Optifine over Forge
2. Install the actual shaders
   * And configure the shaders
3. Install the texture packs
   * And ensure the hierarchy of the textures are correct.


Before moving forward, please ensure there is a `visual mods` folder in your local machine. Within the `visual mods` folder, I structured 3 folders to match how the `.minecraft` folder looks like. 

#

# Tl;dr 
1. Run Optifine jar to install.
2. Copy Optifine into the `mods` folder.
3. Copy all remaining files into their respective location. 
4. Within Minecraft, go to shader options to enable SEUS.
5. Within Minecraft, go to texture pack options to bring in the new textures. [Hierarchy configuration can be found here](https://gitlab.com/yunjang/aeternum-adventures-redux/blob/visual-enhancements/GRAPHICS_INSTALLATION.md#3-install-the-texture-packs)
### As with all of the mods, NONE of the files need to be extracted. Please copy the files over as is. 
#

Remember, you can get to the `.minecraft` folder quicker if you simply type `%appdata%` in the search bar on Window's start menu. 

- `mods` - `PLEASE READ STEP 1 FIRST BEFORE COPYING ANYTHING OVER` Contains additional mods necessary for the visual upgrade to work. 
- `resourcepacks` - Contains all the textures that will cause Minecraft to look different.
- `shaderpacks` - contains the shader mod we will be using as well a `.txt` file. I will get to that later. 

You could just simply copy the contents of the `visual mods` folder into the `.minecraft` folder and all 3 folders should update. But if you want to be safe, go ahead and copy each subfolder into the `.minecraft` folder separately.



# 1. Install Optifine over Forge

1. Go into the contents of the `mods` folder and double click on `OptiFine-1.12.2_HD_U_E3.jar`. You will be prompted to install Optifine. Install it.

![Go ahead and click that install button](https://i.imgur.com/plai5rR.png)

2. Once you install that, go ahead and copy `OptiFine-1.12.2_HD_U_E3.jar` into the `mods` folder within the `.minecraft` folder. 
3. Launch Minecraft with the Forge profile you created in the README instructions. 
4. If optifine installed correctly, you should see Optifine as an additional label on the bottom left of the title screen.

![Optifine on bottom left](https://i.imgur.com/80XFYxC.png)


# 2. Installing the shader

### The `SEUS Renewed 1.0.0.zip.txt` file contains my personal configuration for these shaders. If you choose to copy this file over, you can spend less time configuring and more time crafting.

Please note that from here on out, configuring these options (minus copying them over), are all done IN-GAME. I recommend doing this from the title menu, as opposed to being within the actual game world as it will help with loading.

1. Copy the contents within the `shaderpacks` folder into the `.minecraft` folder. If your `.minecraft` folder does not have a `shaderpacks` folder, something must have messed up in Step 1. Go ahead and start that step over.
2. Optifine gives you a plethora of new options within the settings. But the one we care about the most lies here: `Options... -> Video Settings -> Shaders...` Currently, it is set to `OFF`. Go ahead and click on `SEUS Renewed`. 
   * Note: Your game may freeze while everything updates. Be patient. Might be too late but I recommend not being in full-screen mode so you can have this running in the background while you browse Reddit or something. 
![It should look like this!](https://i.imgur.com/sYP0TBB.png)
3. Keep everything on the right side of the screen as default. 
4. See the `Shader Options...` button? That is how you fine-tune the shaders to make the game look as you want. If you copied over `SEUS Renewed 1.0.0.zip.txt`, then congrats! You're done. No need to do anything. Otherwise, I won't go into detail on every different option. But feel free to play around with them as you see fit.

# 3. Install the texture packs

Unfortunately, I couldn't get everythign to look consistent, but all graphics SHOULD be 32x32 resolution as opposed to Minecraft's default 16x16 resolution.

1. Copy the entire folder contents of the `resourcepacks` folder into the `.minecraft`'s `resourcepacks` folder. 
2. Textures load based on list hierarchy. Take a look at the screenshot below:

![Textures!](https://i.imgur.com/BaTyqnB.png)

3. This is currently my setup. I'll write out the entire list below. Essentially: Anything on that left box will NOT be loaded into Minecraft. If you roll your mouse over any texture pack, you have the option to swap it over to the right window and vice versa.
4. On the right window, the texture pack at the very top will override any conflicting block graphic. Generally, I feel as though texturepacks that try to cover a HUGE variety of blocks should be placed at the bottom, and then anything else that you want fine tuned can be placed above that base texture pack. 
5. Please order the list in the following order:

![Order for textures](https://i.imgur.com/nuJyf34.png)

6. Note how one texture pack is in Red. Don't worry about it. It still works just fine lol.


# Extra: Configurations

Here are my cofigurations for SEUS!
![atmospheric options](https://i.imgur.com/8GJwEGV.png)
![Lighting and Shadows options](https://i.imgur.com/M6Jx1em.png)
![surface options](https://i.imgur.com/LXlFC2u.png)
- Force Wet Surfaces makes the game overall look more "real", but it does come with some issues. Snowy area? You're going to see water ripples on snow. Glass covering a block? the block under the glass will be "wet"
![Advanced GI Options](https://i.imgur.com/qpC6xWL.png)
![Post Processing options](https://i.imgur.com/JPg23Uw.png)



# FAQ

### I don't like the UI changes. Can I disable it?
Unfortunately, yes and no. The UI is tied to `1.12-Dokucraft-TSC-High`. While a lot of textures from this mod actually gets overwritten by `Taunoa Shader Add-On`, you will loose other things like the skybox if you decide to disable this.

### I'm okay with losing some of the nice stuff from Dokucraft. I really dislike the UI.
You can choose to enable `Lithos-Core+v1.21+for+1.12x` instead of `1.12-Dokucraft-TSC-High`. But make sure Lithos Core remains in the same hierarchical position as Dokucraft. Unfortunately, you will most likely see some issues with the blocks not knowing what texture to use. Furthermore, while the main UI will change to a brighter UI, the sub UI stuff will still be dokucraft as I tried to make it as consistent as possible. You can also choose to downright disable everything dokucraft related.

### I found a gross 16x16 texture for some random block! Can you fix that?
No. But you can help by either creating an issue in Gitlab and tagging me, or searching for a texture pack yourself!

### I'm seeing weird graphical glitches. Fix it!
Again, create an issue or ask on LINE. Unfortunately I can't test everything myself, so this will definitely be a team effort. 

### My game crashed after doing X. 
This is most likely NOT a shaders / texture pack issue. You may have gotten super unlucky and caused some dumb Java exception, or some of the mods that affect core gameplay might not talk to each other as nicely as expected. 

#

And that should be it! Hopefully your computer can handle the world's most demanding computer game!